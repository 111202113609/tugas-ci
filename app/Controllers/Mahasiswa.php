<?php
     namespace App\Controllers;


     use CodeIgniter\Controller; //dari json
     use App\Models\Datamodel;

     class Mahasiswa extends Controller
     {
       function __construct()
       {
          $this->data = new Datamodel();
       }

       function index()
       {
         $data['mhs'] = $this->data->getData();
         return view('mahasiswa', $data);
       }

       function edit()
       {
         $data = $this->data->getDtbarang();
         foreach($data->getResult() as $databrg)
         {
            echo $databrg->id, "<br>";
            echo $databrg->nama, "<br>";
            echo $databrg->harga, "<br>";
         }
       }
     }